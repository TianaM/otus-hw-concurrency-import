using System;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Npgsql;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly string _connectionString;

        public CustomerRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void AddCustomer(Customer customer, int attemptNumber = 0)
        {
            //Add customer to data source   
            try
            {
                using (var connection = new NpgsqlConnection(_connectionString))
                {
                    connection.Open();

                    var sql = @"
                            INSERT INTO customers(id, fullname, email, phone) 
                            VALUES (:id, :fullname, :email, :phone) 
                            ";


                    using (var cmd = new NpgsqlCommand(sql, connection))
                    {
                        try
                        {
                            var parameters = cmd.Parameters;
                            parameters.Add(new NpgsqlParameter("id", customer.Id));
                            parameters.Add(new NpgsqlParameter("fullname", customer.FullName));
                            parameters.Add(new NpgsqlParameter("email", customer.Email));
                            parameters.Add(new NpgsqlParameter("phone", customer.Phone));

                            var r = cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            if (attemptNumber < 3)
                                AddCustomer(customer, attemptNumber++);
                            else
                                throw new InvalidOperationException($"Error on insert customer {customer} to DB", ex);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException($"Error on connection to DB {_connectionString}", e);
            }
        }
    }
}