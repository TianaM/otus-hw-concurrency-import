﻿namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public enum ParserFileType
    {
        Default,
        Xml,
        Csv
    }
}
