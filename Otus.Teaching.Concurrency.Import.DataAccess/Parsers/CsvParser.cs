﻿using System.Collections.Generic;
using System.IO;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using ServiceStack.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        private readonly string _fileName;
        public CsvParser(string fileName)
        {
            _fileName = fileName;
        }

        public List<Customer> Parse()
        {
            CustomersList data;
            using (var reader = new FileStream($"{_fileName}", FileMode.Open))
                data = new CustomersList() { Customers = CsvSerializer.DeserializeFromStream<List<Customer>>(reader) };

            return data.Customers;
        }
    }
}
