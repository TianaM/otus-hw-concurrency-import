﻿using System;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public static class ParserFactory
    {
        public static IDataParser<List<Customer>> GetParser(ParserFileType t, string fileName)
        {
            return t switch
            {
                ParserFileType.Xml => new XmlParser($"{fileName}.xml"),
                ParserFileType.Csv => new CsvParser($"{fileName}.csv"),
                _ => throw new ArgumentOutOfRangeException(nameof(t), t, null)
            };
        }
    }
}
