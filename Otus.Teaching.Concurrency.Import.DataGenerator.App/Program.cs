﻿using System;
using System.IO;
//using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFileName; 
        private static int _dataCount = 100;
        private static GeneratorFileType _dataFileType;

        static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;
            
            Console.WriteLine($"Generating {_dataFileType} data...");

            var generator = DataGenerator.Generators.GeneratorFactory.GetGenerator(_dataFileType, _dataFileName, _dataCount);
            
            generator.Generate();
            
            Console.WriteLine($"Generated {_dataFileType} data in {_dataFileName}...");
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                _dataFileName = Path.Combine(_dataFileDirectory, $"{args[0]}.xml");
            }
            else
            {
                Console.WriteLine("Data file name without extension is required");
                return false;
            }

            if (args.Length < 2 || !Enum.TryParse(args[1], true, out _dataFileType))
            {
                Console.WriteLine("File type is not set");
                return false;
            }

            if (args.Length < 3 || !int.TryParse(args[2], out _dataCount))
            {
                Console.WriteLine("Data count must be integer");
                return false;
            }

            return true;
        }
    }
}