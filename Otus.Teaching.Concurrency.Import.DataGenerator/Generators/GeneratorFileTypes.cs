﻿namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public enum GeneratorFileType
    {
        Default,
        Xml,
        Csv
    }
}
