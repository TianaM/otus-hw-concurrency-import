﻿using System;
using Otus.Teaching.Concurrency.Import.Handler.Data;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(GeneratorFileType t, string fileName, int dataCount)
        {
            return t switch
            {
                GeneratorFileType.Xml => new XmlGenerator($"{fileName}.xml", dataCount),
                GeneratorFileType.Csv => new CsvGenerator($"{fileName}.csv", dataCount),
                _ => throw new ArgumentOutOfRangeException(nameof(t), t, null)
            };
        }
    }
}
