﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.Extensions.Configuration;

using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Loader;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

using Npgsql;

namespace Otus.HW.Task6
{
    class Program
    {
        private static IConfiguration _configuration; 
        private static bool _runGeneratorInProc = false;
        private static int _threadCount = 10;
        private static LoaderType _loaderType = LoaderType.Fake;
        private static bool _useThreadPool = true;
        private static GeneratorFileType _generatorFileType = GeneratorFileType.Xml;
        private static ParserFileType _parserFileType = ParserFileType.Xml;
        private static string _generatorFileName = "customers";
        private static int _dataCount = 1000;
        private static string DBName;
        private static string connectionStringSA;
        private static string connectionString;

        static void Main(string[] args)
        {
            ReadInitConfigurationSettings();
            RunGenerator();

            var parser = ParserFactory.GetParser(_parserFileType, $"{_generatorFileName}");
            var customers = parser.Parse();

            InitDB();

            var loader = LoaderFactory.GetLoader(_loaderType, customers, _threadCount, _useThreadPool, connectionString);
            var sw = new Stopwatch();
            sw.Start();
            loader.LoadData();
            sw.Stop();

            if (_useThreadPool)
            Console.WriteLine($"\n{_dataCount} records was loaded in {sw.Elapsed.TotalSeconds} seconds by ThreadPool with {_threadCount} threads");
            else Console.WriteLine($"\n{_dataCount} records was loaded in {sw.Elapsed.TotalSeconds} seconds by {_threadCount} threads");

            Console.ReadLine();
        }

        private static void ReadInitConfigurationSettings()
        {
            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false)
                .Build();

            var generatorSetting = _configuration.GetSection("GeneratorSetting");
            bool.TryParse(generatorSetting.GetSection("RunGeneratorInNewProcess").Value, out _runGeneratorInProc);
            int.TryParse(generatorSetting.GetSection("CountOfGeneratedObject").Value, out _dataCount);

            var loaderSetting = _configuration.GetSection("LoaderSetting");
            Enum.TryParse<LoaderType>(loaderSetting.GetSection("WorkWithDB").Value, true, out _loaderType); 
            bool.TryParse(loaderSetting.GetSection("UseThreadPool").Value, out _useThreadPool);
            int.TryParse(loaderSetting.GetSection("ThreadCount").Value, out _threadCount);

            var fileSetting = _configuration.GetSection("FileSetting");
            Enum.TryParse<GeneratorFileType>(fileSetting.GetSection("FileType").Value, true, out _generatorFileType);
            _generatorFileName = fileSetting.GetSection("FileName").Value;
        }

        private static void RunGenerator()
        {
            if (_runGeneratorInProc)
            {
                var appPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Otus.Teaching.Concurrency.Import.DataGenerator.App.exe");
                Console.WriteLine($"Run new process: {appPath}");
                var proc = Process.Start(appPath, $"{_generatorFileName} {_generatorFileType} {_dataCount}");
                proc?.WaitForExit();
            }
            else
            {
                Console.WriteLine($"Generating {_generatorFileType} data in method...");
                var generator = GeneratorFactory.GetGenerator(_generatorFileType, _generatorFileName, _dataCount);
                generator.Generate();
                Console.WriteLine($"Generated {_generatorFileType} data in {Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _generatorFileName + "." + _generatorFileType)}...");
            }
        }

        private static void InitDB()
        {
            if (_loaderType == LoaderType.PostgreSQL)
            {
                var dbSetting = _configuration.GetSection("DBInitialSettings");
                DBName = dbSetting.GetSection("DBName").Value;
                connectionStringSA = dbSetting.GetSection("ConnectionStringSA").Value;
                connectionString = dbSetting.GetSection("ConnectionString").Value;

                CreateDB();
                CreateCustomersTable();
            }
            else throw new InvalidOperationException($"Error on initialisation DB  {_loaderType}");
        }

        public static void CreateDB()
        {
            using (var connection = new NpgsqlConnection(connectionStringSA))
            {
                connection.Open();

                //using (var transaction = connection.BeginTransaction())
                //{
                //try
                //{
                var sql = @$"SELECT COUNT (*) FROM pg_database WHERE datname = '{DBName}'";
                using (var cmd1 = new NpgsqlCommand(sql, connection))
                {
                    var res = (long)cmd1.ExecuteScalar();

                    if (res == 0)
                    {
                        sql = $@"CREATE DATABASE {DBName}";
                        using (var cmd2 = new NpgsqlCommand(sql, connection))
                        {
                            cmd2.ExecuteNonQuery();
                        }

                    }
                }

                //transaction.Commit();
                //}
                //catch (Exception e)
                //{
                //    //transaction.Rollback();
                //    //Console.WriteLine($"Rolled back the transaction");
                //    return;
                //}
                //}
            }
        }

        public static void CreateCustomersTable()
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                var sql = @"
                            CREATE TABLE IF NOT EXISTS customers
                            (
                                id              INT                         NOT NULL,
                                fullname        CHARACTER VARYING(255)      NOT NULL,
                                email           CHARACTER VARYING(255)      NOT NULL,
                                phone           CHARACTER VARYING(255)      NOT NULL,

                                CONSTRAINT users_pkey PRIMARY KEY (id)
                            );
                        ";

                using (var cmd = new NpgsqlCommand(sql, connection))
                {
                    var affectedRowsCount = cmd.ExecuteNonQuery().ToString();
                }
            }
        }
    }
}
