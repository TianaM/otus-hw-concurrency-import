﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Core.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class PgsqlDataLoader<T> : IDataLoader
    {
        private IEnumerable<T> _data;
        private int _threadCount;
        private bool _useThreadPool; 
        private ICustomerRepository _customerRepository;
        private List<ManualResetEvent> _finishedEvents = new();

        public PgsqlDataLoader(IEnumerable<T> data, int threadCount, bool useThreadPool, ICustomerRepository customerRepository)
        {
            _data = data;
            _threadCount = threadCount;
            _useThreadPool = useThreadPool;
            _customerRepository = customerRepository;
        }

        public void LoadData()
        {
            if (_useThreadPool) ThreadPool.SetMaxThreads(_threadCount, _threadCount);

            int partSize = _data.Count() / _threadCount;
            for (var i = 0; i < _data.Count(); i += partSize)
            {
                var mREvent = new ManualResetEvent(false);
                _finishedEvents.Add(mREvent);

                var length = Math.Min(partSize, _data.Count() - i);
                var part = _data.ToList().GetRange(i, length);

                if (_useThreadPool) ThreadPool.QueueUserWorkItem(PgsqlLoadingData, new Tuple<List<T>, ManualResetEvent>(part, mREvent));
                else new Thread(PgsqlLoadingData).Start(new Tuple<List<T>, ManualResetEvent>(part, mREvent));
            }

            WaitHandle.WaitAll(_finishedEvents.ToArray());
        }

        private void PgsqlLoadingData(object data)
        {
            if (data is not Tuple<List<Customer>, ManualResetEvent> tuple) return;

            var threadId = Thread.CurrentThread.ManagedThreadId;
            var startId = tuple.Item1?.FirstOrDefault()?.Id;
            var endId = tuple.Item1?.LastOrDefault()?.Id;

            Console.WriteLine($"Thread {threadId} started loading customers from {startId} to {endId}.");
            
            foreach(var customer in tuple.Item1)
            { _customerRepository.AddCustomer(customer); }

            Console.WriteLine($"Thread {threadId} finished. Customers ({startId}-{endId}) was loaded succesfully.");

            tuple.Item2.Set();
        }
    }
}