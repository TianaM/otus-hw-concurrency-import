﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public static class LoaderFactory
    {
        public static IDataLoader GetLoader<T>(LoaderType t, IEnumerable<T> data, int threadCount, bool useThreadPool, string connectionString = "")
        {
            return t switch
            {
                LoaderType.Fake => new FakeDataLoader<T>(data, threadCount, useThreadPool),
                LoaderType.PostgreSQL => new PgsqlDataLoader<T>(data, threadCount, useThreadPool, new CustomerRepository(connectionString)),
                _ => throw new ArgumentOutOfRangeException(nameof(t), t, null)
            };
        }
    }
}
