﻿namespace Otus.Teaching.Concurrency.Import.Loader
{
    public enum LoaderType
    {
        Default,
        Fake,
        PostgreSQL
    }
}
