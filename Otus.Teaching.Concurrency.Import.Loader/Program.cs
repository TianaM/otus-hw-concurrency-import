﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        
        static void Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            GenerateCustomersDataFile();

            //var loader = new FakeDataLoader();
            var parser = ParserFactory.GetParser(ParserFileType.Xml, "customers");
            var customers = parser.Parse();
            
            var loader = LoaderFactory.GetLoader(LoaderType.Fake, customers, 10, true); 

            loader.LoadData();
        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 1000);
            xmlGenerator.Generate();
        }
    }
}